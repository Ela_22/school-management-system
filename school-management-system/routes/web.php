<?php

use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::prefix('/admin')->middleware(['auth'])->group(function(){
Route::get('/',function() {

    return view('backend.dashboard');

});
Route::get('/add',[StudentController::class,'create'])->name('student.create');
Route::get('/list',[StudentController::class,'index'])->name('student.index');
Route::post('/store',[StudentController::class,'store'])->name('student.store');
Route::get('/student/{id}',[StudentController::class,'show'])->name('student.show');
Route::get('/student/{id}/edit',[StudentController::class,'edit'])->name('student.edit');
Route::patch('/student/{id}',[StudentController::class,'update'])->name('student.update');
Route::delete('/student/{id}',[StudentController::class,'destroy'])->name('student.destroy');

});