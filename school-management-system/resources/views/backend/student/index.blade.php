<x-backend.layouts.master>

@if(Session::has('message'))
   <p class="text-danger">{{session::get('message')}}</p>
   @endif

<table class='table table-striped' id="table1">
                    <thead>
                        <tr>
                            <th scope="col">Sl</th>
                            <th scope="col">Student Name</th>
                            <th scope="col">Student Class</th>
                            <th scope="col">email</th>
                            <th scope="col">Father Number</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($students as $student)
                        <tr>
                       <th scope="row">{{$loop->iteration}}</th>
                       <td>{{$student->student_name}}</td>
                      <td> {{$student->student_class}}</td>
                      <td>{{$student->email}}</td>
                       <td>{{$student->father_number}}</td>
      <td><a  href="{{route('student.show',['id'=>$student->id])}}"class="btn btn-danger">Show</a>
      <a href="{{route('student.edit',['id'=>$student->id])}}"class="btn btn-warning">Edit</a>
      <form action="{{route('student.destroy',['id'=>$student->id])}}" method="post" style="display:inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-success" onclick="return confirm('Are you sure want to delete?')">Delete</button>
      </form>
      </td>
      
      
    </tr>
    @endforeach
                       
                    </tbody>
                </table>



</x-backend.layouts.master>