<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use Exception;
use PhpParser\Node\Stmt\TryCatch;

use function GuzzleHttp\Promise\all;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::orderBy('id','desc')->get();
        return view('backend.student.index',compact('students'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStudentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentRequest $request)
    
    {
        $data=$request->all();
       
     try {
        Student::create(
        
             
            [
              'student_name'=>$data['student_name'],
              'student_class'=>$data['student_class'],
              'email'=>$data['email'],
              'password'=>$data['password'],
              'father_name'=>$data['father_name'],
              'father_phone'=>$data['father_phone'],
               'mother_name'=>$data['mother_name'],
              'mother_phone'=>$data['mother_phone'],
              'date_of_birth'=>$data['date_of_birth'],
              'gender'=>$data['gender'],
               'adress'=>$data['address']
            ]
            );
     } catch (Exception $ex) {
        // dd($ex);
     }
    
       
            // dd($request);

            return redirect()->route('student.index');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $id)
    {
        $students = Student::findOrFail($id);
        return view('backend.student.show',compact('student'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $students = Student::findOrFail($id);

        return view('backend.student.edit',compact('students'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStudentRequest  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, Student $id)
    {
        $data=$request->all();

        $request->update([

            'student_name'=>$data['student_name'],
              'student_class'=>$data['student_class'],
              'email'=>$data['email'],
              'password'=>$data['password'],
              'father_name'=>$data['father_name'],
              'father_phone'=>$data['father_phone'],
               'mother_name'=>$data['mother_name'],
              'mother_phone'=>$data['mother_phone'],
              'date_of_birth'=>$data['date_of_birth'],
              'gender'=>$data['gender'],
               'adress'=>$data['address']

        ]);
        return redirect()->route('student.index')->withMessage('Successfully Update !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $id)
    {
        $students = Student::findOrfail($id);
        $students->delete();

        return redirect()->route('student.index')->withMessage('Successfully Deleted !');
    }
}
