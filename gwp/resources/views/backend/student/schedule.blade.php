<x-backend.student.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Schedule</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Schedule</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Events
                                <a class="btn btn-sm btn-primary" href="{{ route('schedules.create')}}">Register New Event </a>
                            </div>
                            <div class="card-body">
                                @if(session('message'))
                                <p class="alert alert-success">{{ session('message') }}</p>
                                @endif
                                <div class="col-md-12">
                                <table id="datatablesSimple" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Day</th>
                                            <th>Event</th>
                                          
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($schedules as $schedule)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $schedule->date }} </td>
                                            <td>{{ $schedule->day }} </td>
                                            <td>{{ $schedule->event }} </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
</x-backend.student.layouts.master>