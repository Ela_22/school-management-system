<x-backend.student.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Class Routine</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Class Routine</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Class
                            </div>
                            <div class="card-body">
                                @if(session('message'))
                                <p class="alert alert-success">{{ session('message') }}</p>
                                @endif
                                <div class="col-md-12">
                                <table id="datatablesSimple" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Day</th>
                                            <th>Class 1</th>
                                            <th>Class 2</th>
                                            <th>Class 3</th>
                                            <th>Class 4</th>
                                            <th>Class 5</th>
                                            <th>Class 6</th>
                                            <th>Class 7</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <tr>
                                            <td>{{ 'Sunday' }} </td>
                                            <td>{{ 'English' }} </td>
                                            <td>{{ 'Bengali' }} </td>
                                            <td>{{ 'Maths' }} </td>
                                            <td>{{ 'Physics' }} </td>
                                            <td>{{ 'Chemistry' }} </td>
                                            <td>{{ 'Biology' }} </td>
                                            <td>{{ 'Pure Maths' }} </td>    
                                        </tr>

                                        <tr>
                                            <td>{{ 'Monday' }} </td>
                                            <td>{{ 'English' }} </td>
                                            <td>{{ 'Bengali' }} </td>
                                            <td>{{ 'Maths' }} </td>
                                            <td>{{ 'Physics' }} </td>
                                            <td>{{ 'Chemistry' }} </td>
                                            <td>{{ 'Biology' }} </td>
                                            <td>{{ 'Pure Maths' }} </td>    
                                        </tr>

                                        <tr>
                                            <td>{{ 'Tuesday' }} </td>
                                            <td>{{ 'English' }} </td>
                                            <td>{{ 'Bengali' }} </td>
                                            <td>{{ 'Maths' }} </td>
                                            <td>{{ 'Physics' }} </td>
                                            <td>{{ 'Chemistry' }} </td>
                                            <td>{{ 'Biology' }} </td>
                                            <td>{{ 'Pure Maths' }} </td>    
                                        </tr>

                                        <tr>
                                            <td>{{ 'Wednesday' }} </td>
                                            <td>{{ 'English' }} </td>
                                            <td>{{ 'Bengali' }} </td>
                                            <td>{{ 'Maths' }} </td>
                                            <td>{{ 'Physics' }} </td>
                                            <td>{{ 'Chemistry' }} </td>
                                            <td>{{ 'Biology' }} </td>
                                            <td>{{ 'Pure Maths' }} </td>    
                                        </tr>

                                        <tr>
                                            <td>{{ 'Thursday' }} </td>
                                            <td>{{ 'English' }} </td>
                                            <td>{{ 'Bengali' }} </td>
                                            <td>{{ 'Maths' }} </td>
                                            <td>{{ 'Physics' }} </td>
                                            <td>{{ 'Chemistry' }} </td>
                                            <td>{{ 'Biology' }} </td>
                                            <td>{{ 'Pure Maths' }} </td>    
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
</x-backend.student.layouts.master>