<x-backend.student.layouts.master>

    <h1 class="mt-4">Student</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Product Details
            <a class="btn btn-sm btn-primary" href="{{ route('students.index') }}">Dashboard</a>
        </div>
        <div class="card-body">
            <h3>Name: {{ $student->student_name }}</h3>
            <h3>Class: {{ $student->student_class }}</h3>
            <p>Email: {{ $student->email }}</p>
            <p>Date of Birth: {{ $student->date_of_birth }}</p>
            <p>Gender: {{ $student->gender }}</p>
            <p>Father's Name: {{ $student->father_name }}</p>
            <p>Father's Phone: {{ $student->father_phone }}</p>
            <p>Mother's Name: {{ $student->mother_name }}</p>
            <p>Mother's Phone: {{ $student->mother_phone }}</p>
            <p>Address: {{ $student->address }}</p>
            
            
        </div>

    </div>

</x-backend.student.layouts.master>