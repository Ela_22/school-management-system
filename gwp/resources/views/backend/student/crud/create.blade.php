<x-backend.student.layouts.master>

<div class="container">
<h1>Student Information</h1>
<form action="{{route('students.store')}}"method="post"style="justify-content: center;width: 100%; font-weight: bold;">
   @csrf

      <div class="row" style="margin-top:50px; margin-bottom: 10px;">
        <div class="col">
          <label>Student Name:</label>
          <input name="student_name" type="text" class="form-control" placeholder="Enter full name" aria-label="First name">
        </div>
        <div class="col">

          <label>Student Class  </label>
          <input name="student_class"type="text" class="form-control" placeholder="student class" >

        </div>
      </div>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col">
          <label for=""> email:</label>
          <input name="email" type="email" class="form-control" >
        </div>
        <div class="col">
          <label>Password:</label>
          <input name="password"type="password" class="form-control" aria-label="password">
        </div>
      </div>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col">
          <label for="">Father Name</label>
          <input name="father_name" type="text" class="form-control" aria-label="First name">
        </div>
        <div class="col">
          <label for="phone"> phone number</label>
          <input name="father_phone" type="number" class="form-control" placeholder="(+880)" aria-label="Last name">
        </div>
      </div>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col">
          <label for="">Mother Name</label>
          <input name="mother_name"type="text" class="form-control" >
        </div>
        <div class="col">
          <label for="phone"> phone number</label>
          <input name="mother_phone" type="number" class="form-control" placeholder="(+880)" >
        </div>
      </div>

       <div class="row" style="margin: bottom 10px;">
      
      <div class="col">
          <label for="dateofbirth">Date of birth:</label>
          <div class="row">
            <input name="date_of_birth" type="date">
          </div>
      </div>
        <div class="col">
          <label for="gender">Gender:</label>
          <select name="gender" class="form-select" aria-label="Default select example" id="gender">
            <option selected>Select your Gender</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
</div>

     
      <!-- <div class="row" style="margin-bottom: 10px;"> -->
        <div class="row ">
          <label for="adress">Adress:</label>
          <div class="row">
          
              <textarea name="address" id="" cols="50" rows="2"></textarea></div>
        
        </div>
      <div class="mb-3">
  <label for="formFile" class="form-label">Student Picture:</label>
  <input class="form-control" type="file" id="image">
</div>
      <div class="row justify-content-center">
        <div class="col-auto ">
          <button type="submit" class="btn btn-primary"
            style="width: 200px; font-weight: bold; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; margin-bottom: 50px; margin-top: 20px;">Submit</button>
  
        </div>
        <br>
      </div>
   
      
    



    </form>
</div>

</x-backend.student.layouts.master>