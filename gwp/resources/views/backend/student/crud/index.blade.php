<x-backend.student.layouts.master>

@if(Session::has('message'))
   <p class="text-danger">{{session::get('message')}}</p>
   @endif
<h3><a href="{{ route('dashboard.student') }}">Dashboard</a></h3><br> 
<h4><a href="{{ route('students.create') }}">Add Details</a></h4>
<table class='table table-striped' id="table1">
    <thead>
        <tr>
            <th scope="col">Sl</th>
            <th scope="col">Student Name</th>
            <th scope="col">Student Class</th>
            <th scope="col">email</th>
            <th scope="col">Father Number</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($students as $student)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$student->student_name}}</td>
            <td> {{$student->student_class}}</td>
            <td>{{$student->email}}</td>
            <td>{{$student->father_phone}}</td>

            <td><a  href="{{ route('students.show', ['student' => $student->id]) }}"class="btn btn-danger">Show</a>
            
            <a href="{{ route('students.edit', ['student' => $student->id]) }}"class="btn btn-warning">Edit</a>
            
            <form action="{{ route('students.destroy', ['student' => $student->id]) }}" method="post" style="display:inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-success" onclick="return confirm('Are you sure want to delete?')">Delete</button>
            </form>
            </td>
        </tr>
        @endforeach
                        
    </tbody>
</table>



</x-backend.student.layouts.master>