<x-backend.teacher.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Class Routine</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Class Routine</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Class
                            </div>
                            <div class="card-body">
                                @if(session('message'))
                                <p class="alert alert-success">{{ session('message') }}</p>
                                @endif
                                <div class="col-md-12">
                                <table id="datatablesSimple" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Day</th>
                                            <th>8 AM</th>
                                            <th>9 AM</th>
                                            <th>10 AM</th>
                                            <th>11 AM</th>
                                            <th>12 PM</th>
                                            <th>1 PM</th>
                                            <th>2 PM</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <tr>
                                            <td>{{ 'Sunday' }} </td>
                                            <td>{{ 'English, Class 3' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Maths, Class 1' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Chemistry, Class 7' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Pure Maths, Class 9' }} </td>    
                                        </tr>

                                        <tr>
                                            <td>{{ 'Monday' }} </td>
                                            <td>{{ 'English, Class 3' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Maths, Class 1' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Chemistry, Class 7' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Pure Maths, Class 9' }} </td>     
                                        </tr>

                                        <tr>
                                            <td>{{ 'Tuesday' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Maths, Class 5' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Chemistry, Class 7' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Pure Maths, Class 9' }} </td>
                                            <td>{{ 'Break' }} </td>    
                                        </tr>

                                        <tr>
                                            <td>{{ 'Wednesday' }} </td>
                                            <td>{{ 'English, Class 3' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Maths, Class 1' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Chemistry, Class 7' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Pure Maths, Class 9' }} </td>    
                                        </tr>

                                        <tr>
                                            <td>{{ 'Thursday' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Maths, Class 5' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Chemistry, Class 7' }} </td>
                                            <td>{{ 'Break' }} </td>
                                            <td>{{ 'Pure Maths, Class 9' }} </td>
                                            <td>{{ 'Break' }} </td>      
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
</x-backend.teacher.layouts.master>