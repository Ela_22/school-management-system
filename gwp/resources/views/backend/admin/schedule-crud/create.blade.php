<x-backend.admin.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Schedule</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Schedule</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Register New Event
                                <a class="btn btn-sm btn-primary" href="{{ route('schedules.index')}}"> Schedule Table</a>
                            </div>
                            <div class="card-body">

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('schedules.store')}}" method="POST">
    @csrf
                
                <div class="mb-3">
                    <label for="date" class="form-label">Date</label>
                    <input name="date" type="date" class="form-control" id="date" value="{{old('date')}}" >
                    
                    @error('date')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="day" class="form-label">Day</label>
                    <input name="day" type="text" class="form-control" id="day" value="{{old('day')}}" >
                    
                    @error('day')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="event" class="form-label">Event</label>
                    <input name="event" type="text" class="form-control" id="event" value="{{old('event')}}" >
                    
                    @error('event')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="month" class="form-label">Month</label>
                    <input name="month" type="month" class="form-control" id="month" value="{{old('month')}}" >
                    
                    @error('event')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
  
                <button type="submit" class="btn btn-primary">Save</button>
</form>
                            </div>
                        </div>
                   
</x-backend.admin.layouts.master>