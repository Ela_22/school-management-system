<x-backend.admin.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Teachers</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Teachers</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Teacher Details
                                <a class="btn btn-sm btn-primary" href="{{ route('teachers.index')}}">Teacher List </a>
                            </div>
                            <div class="card-body">

                               
                               <h3> Name: {{$teacher->name}}</h3>
                               <p> Subject: {{$teacher->subject}}</p>
                                <p> Contact: {{ $teacher->contact }} </p>
                               <p> Email: {{ $teacher->email }} </p>
                               <p> Present Address: {{ $teacher->present_address }} </p>
                               <p> Permanent Address: {{ $teacher->permanent_address }} </p>
                               <p> Designation: {{ $teacher->designation }} </p>
                               <p> Educational Qualification: {{ $teacher->educational_qualification }} </p>
                               <p> Joining Date: {{ $teacher->joining_date }} </p>
                              
                            </div>
                        </div>
                    </div>
</x-backend.admin.layouts.master>