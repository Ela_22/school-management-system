<x-backend.principal.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Earnings & Expense</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Earnings & Expense</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Register New Event
                                <a class="btn btn-sm btn-primary" href="{{ route('deals.index')}}"> Business Records</a>
                            </div>
                            <div class="card-body">

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('deals.store')}}" method="POST">
    @csrf
                
                <div class="mb-3">
                    <label for="name" class="form-label">Transactions</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{old('name')}}" >
                    
                    @error('name')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="amount" class="form-label">Amount</label>
                    <input name="amount" type="number" class="form-control" id="amount" value="{{old('amount')}}" >
                    
                    @error('amount')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="dealtype" class="form-label">Deal Type</label>
                    <select name="dealtype" type="text" class="form-control" id="dealtype">
                        <option value="{{'Earnings'}}">Earnings</option>
                        <option value="{{'Expense'}}">Expense</option>
                    </select>
                    
                    @error('dealtype')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
  
                <button type="submit" class="btn btn-primary">Save</button>
</form>
                            </div>
                        </div>
</x-backend.principal.layouts.master>