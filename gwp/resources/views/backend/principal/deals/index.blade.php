<x-backend.principal.layouts.master>
<div class="container-fluid px-4">
    <h1 class="mt-4">Earnings & Expense</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Earnings & Expense</li>
    </ol>
    
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Events
            <a class="btn btn-sm btn-primary" href="{{ route('deals.create')}}">Register New Transaction </a>
        </div>
        <div class="card-body">
            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif
            <div class="col-md-12">
            <table id="datatablesSimple" style="width: 100%">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Transactions</th>
                        <th>Amount</th>
                        <th>Deal Type</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($deals as $deal)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $deal->name }} </td>
                        <td>{{ $deal->amount }} </td>
                        <td>{{ $deal->dealtype }} </td>
                        
                        
                        <td>
                            <a class="btn btn-warning btn-sm" href="{{route('deals.edit', ['deal' => $deal->id]) }}"> Edit </a>
                            
                            <form action="{{ route('deals.destroy', ['deal' => $deal->id]) }}" method="POST" style="display:inline">
                            @csrf
                            @method('delete') 
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                            </form>
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
</x-backend.principal.layouts.master>
