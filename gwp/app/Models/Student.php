<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable =[

        'student_name',
        'student_class',
        'email',
        'password',
        'father_name',
        'mother_name',
        'father_phone',
        'mother_phone',
        'date_of_birth',
        'gender',
        'adress',
    ];

    
}
