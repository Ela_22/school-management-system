<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::all();   
        return view('backend.admin.schedule-crud.index', compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.schedule-crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            Schedule::create($request->all());   
 
            return redirect()->route('schedules.index')->withMessage('Successfully Saved !');

        }
        catch(QueryException $e)
        {
             return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        return view('backend.admin.schedule-crud.show', compact('schedule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        return view('backend.admin.schedule-crud.edit', compact('schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        try
        { 
            $schedule->update([
                'date' => $request->date,
                'day' => $request->day,
                'event' => $request->event,
                'month' => $request->month
            ]);

            return redirect()->route('schedules.index')->withMessage('Successfully Updated!');
       }
       catch (QueryException $e)
       {
           return redirect()->back()->withInput()->withErrors($e->getMessage());
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->delete();
        return redirect()->route('schedules.index')->withMessage('Successfully Deleted !');
    }
}
