<?php

namespace App\Http\Controllers;

use App\Models\Staff;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = Staff::orderBy('id', 'desc')->get();
     
        return view('backend.admin.staff-crud.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.staff-crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            Staff::create($request->all());   
 
            return redirect()->route('staffs.index')->withMessage('Successfully Saved !');

        } catch(QueryException $e){
             return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = Staff::findOrFail($id);                                     
        return view('backend.admin.staff-crud.show', compact('staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = Staff::findOrFail($id);
        return view('backend.admin.staff-crud.edit', compact('staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{ 
            $staff = Staff::findOrFail($id);
                $staff->update([
                    
                    'name' => $request->name,
                    'contact' => $request->contact,
                    'email' => $request->email,
                    'present_address' => $request->present_address,
                    'parmanent_address' => $request->permanent_address,
                    'designation' => $request->designation,
                    'educational_qualification' => $request->educational_qualification,
                ]);
                
   return redirect()->route('staffs.index')->withMessage('Successfully Updated!');

       }catch (QueryException $e){
           return redirect()->back()->withInput()->withErrors($e->getMessage());

       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff::findOrFail($id);
        $staff->delete();
        return redirect()->route('staffs.index')->withMessage('Successfully Deleted !');
    }
}
