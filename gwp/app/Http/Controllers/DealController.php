<?php

namespace App\Http\Controllers;

use App\Models\Deal;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deals = Deal::all();
        return view('backend.principal.deals.index', compact('deals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.principal.deals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            Deal::create($request->all());   
 
            return redirect()->route('deals.index')->withMessage('Successfully Saved !');

        }
        catch(QueryException $e)
        {
             return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Deal $deal)
    {
        return view('backend.principal.deals.edit', compact('deal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deal $deal)
    {
        try
        { 
            
            $deal->update([
                'name' => $request->name,
                'amount' => $request->amount,
                'dealtype' => $request->dealtype
            ]);

            return redirect()->route('deals.index')->withMessage('Successfully Updated!');
       }
       catch (QueryException $e)
       {
           return redirect()->back()->withInput()->withErrors($e->getMessage());
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deal $deal)
    {
        $deal->delete();
        return redirect()->route('schedules.index')->withMessage('Successfully Deleted !');
    }
}
