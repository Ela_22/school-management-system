<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use Illuminate\Http\Request;

class PrincipalController extends Controller
{
    public function schedule()
    {
        $schedules = Schedule::all();
        return view('backend.principal.schedule', compact('schedules'));
    }
}
